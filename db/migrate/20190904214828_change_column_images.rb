class ChangeColumnImages < ActiveRecord::Migration[5.2]
  def change
    change_column :images, :b64, 'bytea USING CAST(b64 AS bytea)', :limit => 10.megabytes
  end
end
