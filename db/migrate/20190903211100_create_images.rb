class CreateImages < ActiveRecord::Migration[5.2]
  def change
    create_table :images do |t|
      t.string :filename
      t.string :b64
      t.string :description

      t.timestamps
    end
  end
end
