var ImagesList = createReactClass({

    render() {
        return (
            <div>
                {
                    this.props.images.map((item, index) => {
                        return (
                                <Image key={item.id} image={item} />
                        )
                    })
                }
            </div>
        )
    }
});