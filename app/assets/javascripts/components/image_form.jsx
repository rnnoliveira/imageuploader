const ImageForm = createReactClass({

    handleChange(e) {
        let name = e.target.name;
        obj = {};
        obj[name] = e.target.value;
        this.props.onUserInput(obj);
    },

    handleImageChange(e) {
        e.preventDefault();
        this.props.onFile(e.target.files[0]);
    },

    handleSubmit(e) {
        e.preventDefault();
        this.props.onFormSubmit();
    },


    render() {
        return (
            <div>
                <h1>Image Uploader</h1>
                <h2>Upload a new image</h2>
                <form onSubmit={this.handleSubmit}>
                    <input name="filename" placeholder='Image name'
                           value={this.props.filename}
                           onChange={this.handleChange}/>

                    <input name="description" placeholder='Image description'
                           value={this.props.descripition}
                           onChange={this.handleChange}/>

                    <div className="row">
                        <input name="b64" type="file"
                              onChange={this.handleImageChange}
                              />
                    </div>

                    <div className="row">
                        <input type="submit" value='Upload image'/>
                    </div>

                </form>
            </div>
        );
    }
});