const Images = createReactClass({

    getInitialState() {
        return {
            images: this.props.images,
            filename: '',
            description: '',
            file: '',
            b64: ''
        };
    },

    handleUserInput(obj) {
        this.setState(obj);
    },


    handleFile(obj) {
        const reader = new FileReader();
        reader.readAsDataURL(obj);
        reader.onloadend = () => {
            this.setState({b64: reader.result})
        }

    },


    handleFormSubmit: function () {
        let image = {filename: this.state.filename, description: this.state.description, b64: this.state.b64, file: this.state.b64};
        $.post('/images',
            {image: image});
        this.updateImageList(image)
    },

    updateImageList(image) {
        let newState = this.state.images.concat(image);
        this.setState({images: newState});
    },


    render() {
        return (
            <div>
                <ImageForm filename={this.state.filename}
                           description={this.state.description}
                           onUserInput={this.handleUserInput}
                           onFormSubmit={this.handleFormSubmit}
                           onFile={this.handleFile}/>

                <ImagesList images={this.state.images}/>
            </div>
        );
    }
});


