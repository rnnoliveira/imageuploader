const Image = createReactClass({
    render() {
        return (
            <div className="row">
                <div className="column">
                    <h3>{this.props.image.filename}</h3>
                    <img
                        src={"data:image/jpeg;" + this.props.image.b64}
                        border="2" width="158" height="150" hspace="10"/>
                    <p>{this.props.image.description}</p>
                </div>
            </div>);
    }
});