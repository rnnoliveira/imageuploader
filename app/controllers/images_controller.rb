class ImagesController < ApplicationController

  before_action :authenticate_user!

  def index
    @images = current_user.images.order('created_at ASC')
    @image = Image.new
  end

  def create

    @image = Image.new(image_params)
    @image.user = current_user

    respond_to do |format|
      if @image.save
        format.json { render json: @image, status: :created }
      else
        format.json { render json: @image.errors, status: :unprocessable_entity }
      end

    end

  end

  private

  def image_params
    params.require(:image).permit!
  end


end
