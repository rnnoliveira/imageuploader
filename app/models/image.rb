class Image < ApplicationRecord

  mount_base64_uploader :file, FileUploader

  belongs_to :user

  validates_presence_of :filename, :description, :b64, :file

end
