## First Install

### Before running

* [Install Docker ](https://www.docker.com/products/docker-desktop)

### Then 

`docker-compose up -d`

`docker-compose run web rails db:create db:migrate`

### Running tests

`docker-compose run web bundle exec rspec`

### Ruby Version

* 2.5.3

### Gems

* gem 'rails', '~> 5.2.3'
* gem 'react-rails'
* gem 'devise'
* gem 'carrierwave', '~> 2.0'
* gem 'jquery-rails'
* gem 'haml'
* gem "fog-aws"
* gem 'dotenv-rails'
* gem 'rspec-rails', '~> 3.8'
* gem 'shoulda-matchers'