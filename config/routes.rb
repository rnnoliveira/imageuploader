Rails.application.routes.draw do
  devise_for :users
  get 'images/index'
  root 'images#index'
  resources :images
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
