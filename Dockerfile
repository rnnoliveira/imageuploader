FROM ruby:2.5.3
RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs
RUN mkdir -p /app-devolute
WORKDIR /app-devolute
COPY Gemfile /app-devolute/Gemfile
COPY Gemfile.lock /app-devolute/Gemfile.lock
COPY . /app-devolute
RUN bundle install